# Deployment

We are assuming that we want to deploy our stack on a kubernetes cluster and we have referred to kubernetes resources (components) as means of representing machines and deployed workloads.

Below, you are seeing a very basic overview of our deployment flow.

<p align="center">
  <img width="460" height="300" src="assets/k8s-cluster-overview.png">
</p>


As mentioned before in other documents, we have devided our whole stack to two different Big Components, `Publishers` and `Consumers`. Each of these components should be able to perform independently and serve its users properly. There are some data integration flows between these to components which we have described in `arch` documents.

### Publishers

<p align="center">
  <img src="assets/publishers-overview.png">
</p>

### Consumers

<p align="center">
  <img src="assets/consumers-overview.png">
</p>

### Data Integration

<p align="center">
  <img src="assets/data-integration-overview.png">
</p>

## Production

### Publishers

* Stateful Workloads
  * Main Database (PostgreSQL)
    * Single Node
  * Index Engine (ElasticSearch)
    * Single Node
  * Message Broker (RabbitMQ)
    * Single Node
  * Object Storage (S3)
    * Cloud

* Stateless Workloads
  * `pbackend`
    * HA Deployment
    * Rolling Update - `3` live pods
    * `>=5` instances (pods)
  * `pprocessor`
    * Workers
    * CPU Bound
    * `>=5` instances - deployed as different workers on `1` pod
  * `pclient`
    * HA Deployment
    * Rolling Update - `3` live pods
    * `>=5` instances (pods)

### Consumers

* Stateful Workloads
  * Main Database (MySQL)
    * Clustered Deployment
  * Index Engine (ElasticSearch)
    * Clustered Deployment
  * Object Storage (S3)
    * Cloud

* Stateless Workloads
  * `cclient` (Store Front)
    * HA Deployment
    * Rolling Update - `5` live pods
    * `>=10` instances (pods)
  * `cbackend`
    * HA Deployment
    * Rolling Update - `10` live pods
    * `>=20` instances (pods)


## Development (Staging)

### Publishers

* Stateful Workloads
  * Main Database (PostgreSQL)
    * Single Node
  * Index Engine (ElasticSearch)
    * Single Node
  * Message Broker (RabbitMQ)
    * Single Node
  * Object Storage (S3)
    * Cloud

* Stateless Workloads
  * `pbackend`
    * HA Deployment
    * Rolling Update - `1` live pods
    * `>=3` instances (pods)
  * `pprocessor`
    * Workers
    * CPU Bound
    * `>=2` instances - deployed as different workers on `1` pod
  * `pclient`
    * HA Deployment
    * Rolling Update - `1` live pods
    * `>=3` instances (pods)

### Consumers

* Stateful Workloads
  * Main Database (MySQL)
    * Single Node
  * Index Engine (ElasticSearch)
    * Single Node
  * Object Storage (S3)
    * Cloud

* Stateless Workloads
  * `cclient` (Store Front)
    * HA Deployment
    * Rolling Update - `1` live pods
    * `>=3` instances (pods)
  * `cbackend`
    * HA Deployment
    * Rolling Update - `1` live pods
    * `>=3` instances (pods)


# Monitoring

## Error/Issue Tracking
* Sentry
  * Integrate with stateless services:
    * cclient
    * cbackend
    * pbackend
    * pprocessor
    * pclient

## SLOs, Access Monitor, Health Check
* ELK Stack
* Prometheus
  * Integrate with all workloads
  * Install proper exporters for different entities
  * `Grafana` for visualization
