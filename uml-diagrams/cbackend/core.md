```mermaid
classDiagram
    class Author
    Author : +String first_name
    Author : +String middle_name
    Author : +String last_name
    Author : +Date date_of_birth
    Author : +URL avatar
    Author : +String about

    class Publisher
    Publisher : +String organization_name
    Publisher : +String founder_name
    Publisher : +String country
    Publisher : +Date founding_date

    class Book
    Book : +String primary_title
    Book : +String secondary_title
    Book : +String description
    Book : +Integer publisher_id
    Book : +Integer length
    Book : +String asin_code
    Book : +String isbn_code
    Book : +String language_code
    Book : +Boolean is_self_published

    class BookAsset
    BookAsset : +String storage_url
    BookAsset : +String title
    BookAsset : +String size
    BookAsset : +String extension

    class PaymentMethod
    PaymentMethod : +String title
    PaymentMethod : +Integer amount
    PaymentMethod : +String unit
    PaymentMethod : +String payment_type
    PaymentMethod : +Boolean is_free

    class AssetPaymentAssociation
    AssetPaymentAssociation : +FK book_id
    AssetPaymentAssociation : +FK asset_id
    AssetPaymentAssociation : +FK payment
    AssetPaymentAssociation : +String status

    Book --> "many" BookAsset : Contains
    BookAsset "1" --> "1" PaymentMethod : AssetPaymentAssociation

    Publisher --> "many" Book : has
    Author --> "many" Book : has


```

```mermaid
classDiagram
    class KindleUser
    KindleUser : +String access_token
    KindleUser : +String refresh_token
    KindleUser : +String first_name
    KindleUser : +String middle_name
    KindleUser : +String last_name
    KindleUser : +String email
    KindleUser : +Integer associated_account_id
    KindleUser : +Boolean is_self_publisher

    class Address
    Address : +FK user_id
    Address : +String country
    Address : +String state
    Address : +String city
    Address : +String line1
    Address : +String line2

    class OrderItem
    OrderItem : +FK product_id
    OrderItem : +FK order_id
    OrderItem : +FK shopping_cart_id
    OrderItem : +Integer quantity
    OrderItem : +Integer price

    class Order
    Order : +FK address_id
    Order : +Boolean is_ordered
    Order : +Boolean is_shipped
    Order : +String status
    Order : +Date creation_date

    class Payment
    Payment : +Boolean is_paid
    Payment : +Integer total_amount
    Payment : +JSON extra_information
    Payment : +Date payment_date

    class ShoppingCart
    ShoppingCart : +Date creation_date

    KindleUser *-- Order
    KindleUser *-- ShoppingCart
    KindleUser *-- Payment

    Order "1" -- "*" OrderItem
    ShoppingCart "1" -- "*" OrderItem
    Order "1" -- "*" Payment
    Order "*" -- "1" Address
```

```mermaid
classDiagram
    class BookAssetAccessGrant
    BookAssetAccessGrant : +FK kindle_user_id
    BookAssetAccessGrant : +FK book_id
    BookAssetAccessGrant : +FK book_asset_id
    BookAssetAccessGrant : +Boolean is_unlimited
    BookAssetAccessGrant : +Boolean is_expired
    BookAssetAccessGrant : +Date creation_date
    BookAssetAccessGrant : +Date expiration_date

    class ReadingTracker
    ReadingTracker : +FK kindle_user_id
    ReadingTracker : +FK book_asset_access_grant_id
    ReadingTracker : +Integer current_page
    ReadingTracker : +[]Integer skipped_pages
    ReadingTracker : +Integer spent_hours
    ReadingTracker : +Boolean is_started
    ReadingTracker : +Boolean is_completed
    ReadingTracker : +Date last_modification_date
    ReadingTracker : +Date completion_date

    KindleUser *-- BookAssetAccessGrant
    Book *-- BookAssetAccessGrant
    BookAsset *-- BookAssetAccessGrant
    BookAssetAccessGrant *-- ReadingTracker
```