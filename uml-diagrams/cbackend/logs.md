```mermaid
classDiagram
    class SearchLog
    SearchLog : +FK kindle_user_id
    SearchLog : +String raw_query
    SearchLog : +String analyzed_query
    SearchLog : +Integer page
    SearchLog : +[]Integer books_in_results
    SearchLog : +Date creation_date

    class SearchClickLog
    SearchClickLog : +FK kindle_user_id
    SearchClickLog : +FK search_log_id
    SearchClickLog : +Integer clicked_book_id
    SearchClickLog : +Integer rank
    SearchClickLog : +Date creation_date

    class ReadingAttentionLog
    ReadingAttentionLog : +FK kindle_user_id
    ReadingAttentionLog : +FK reading_tracker_id
    ReadingAttentionLog : +Integer page
    ReadingAttentionLog : +Integer spent_time
    ReadingAttentionLog : +Boolean is_stopped
```