```mermaid
classDiagram
    class ExtendedUser
    ExtendedUser : +String first_name
    ExtendedUser : +String middle_name
    ExtendedUser : +String last_name
    ExtendedUser : +String email

    class OAuthApplication
    OAuthApplication : +FK extended_user_id
    OAuthApplication : +String name
    OAuthApplication : +String client_id
    OAuthApplication : +String client_secret
    OAuthApplication : +[]String redirect_uris

    class OAuthAccessToken
    OAuthAccessToken : +FK user_id
    OAuthAccessToken : +String token
    OAuthAccessToken : +Date expiration_date
    OAuthAccessToken : +FK refresh_token_id

    class OAuthRefreshToken
    OAuthRefreshToken : +FK user_id
    OAuthRefreshToken : +String token
    OAuthRefreshToken : +Date expiration_date

    OAuthApplication --> "many" OAuthAccessToken
    ExtendedUser --> "many" OAuthApplication : app owner
    ExtendedUser --> "many" OAuthAccessToken : resource owner
    ExtendedUser --> "many" OAuthRefreshToken : resource owner
    OAuthApplication --> "many" OAuthRefreshToken
```
