```mermaid
classDiagram
    class KindleUser
    KindleUser : +String access_token
    KindleUser : +String refresh_token
    KindleUser : +String first_name
    KindleUser : +String middle_name
    KindleUser : +String last_name
    KindleUser : +String email
    KindleUser : +Integer associated_account_id
    KindleUser : +Boolean is_self_publisher

    class Author
    Author : +FK kindle_user_id
    Author : +String first_name
    Author : +String middle_name
    Author : +String last_name
    Author : +Date date_of_birth
    Author : +URL avatar
    Author : +String about

    class Publisher
    Publisher : +FK kindle_user_id
    Publisher : +String organization_name
    Publisher : +String founder_name
    Publisher : +String country
    Publisher : +Date founding_date

    class Book
    Book : +String primary_title
    Book : +String secondary_title
    Book : +String description
    Book : +Integer publisher_id
    Book : +Integer length
    Book : +String asin_code
    Book : +String isbn_code
    Book : +String language_code
    Book : +Boolean is_self_published

    class BookAsset
    BookAsset : +String storage_url
    BookAsset : +String title
    BookAsset : +String size
    BookAsset : +String extension

    class PaymentMethod
    PaymentMethod : +String title
    PaymentMethod : +Integer amount
    PaymentMethod : +String unit
    PaymentMethod : +String payment_type
    PaymentMethod : +Boolean is_free

    class AssetPaymentAssociation
    AssetPaymentAssociation : +FK book_id
    AssetPaymentAssociation : +FK asset_id
    AssetPaymentAssociation : +FK payment
    AssetPaymentAssociation : +String status

    Book --> "many" BookAsset : Contains
    BookAsset "1" --> "1" PaymentMethod : AssetPaymentAssociation

    Publisher --> "many" Book : has
    Author --> "many" Book : has

    Publisher --> "many" Author : has

    KindleUser o-- Publisher : associated
    KindleUser o-- Author : associated
```