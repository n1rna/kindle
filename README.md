Kindle Arch Challenge Assignment
===

> Reynen Court. Python Developer Interview Assignment
> 
> *February - March 2020*


Assignee: **Nima Yazdanmehr**

Superviosr: **Raghav Salotra**

You can find the `pdf` file for this asssignment [here](https://gitlab.com/n1rna/kindle/-/blob/master/Kindle%20Arch%20Challenge.pdf).
