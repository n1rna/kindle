from django.db import models
from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    UserManager
)
from django.utils.crypto import get_random_string

# Create your models here.


class ExtendedUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField('Username', max_length=32, unique=True)
    first_name = models.CharField('First Name', max_length=128, null=True, blank=True)
    middle_name = models.CharField('Middle Name', max_length=128, null=True, blank=True)
    last_name = models.CharField('Last Name', max_length=128, null=True, blank=True)
    email = models.EmailField('Email')
    is_staff = models.BooleanField('Is Staff', default=False)
    is_active = models.BooleanField('Is Active', default=True)
    creation_date = models.DateTimeField('Creation Date', auto_now_add=True)
    date_joined = models.DateTimeField('Date Joined', auto_now_add=True)

    is_email_confirmed = models.BooleanField('Is Email Confirmed', default=False)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __unicode__(self):
        return self.username

    def __str__(self):
        return self.username

    def get_short_name(self):
        return self.first_name

    def get_username(self):
        return self.username

    def get_email_field_name():
        return 'email'

    def email_user(self, *kwargs):
        return True

    @property
    def complete_name(self):
        return self.first_name + ' ' + self.last_name


class EmailVerificationToken(models.Model):
    email = models.EmailField('Email', unique=False)
    token = models.CharField(max_length=256, null=True, blank=True)

    is_verified = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)

    creation_date = models.DateTimeField('Creation Date', auto_now_add=True)
    verification_date = models.DateTimeField('Verification date', null=True, blank=True)

    def generate_token(self, length=16):
        return get_random_string(length)

    def save(self, *args, **kwargs):
        if not self.token:
            token = self.generate_token(16)
            self.token = token
        return super(EmailVerificationToken, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return f'http://{settings.SERVICE_BASE_URL}/verify-email/?e={self.email}&t={self.token}'

    @classmethod
    def create_token_for_email(cls, email):
        cls.objects.filter(email=email).update(is_expired=True)
        return cls.objects.create(email=email)


class PasswordResetToken(models.Model):
    email = models.CharField(max_length=256, null=True, blank=True)
    token = models.CharField(max_length=256, null=True, blank=True)

    is_expired = models.BooleanField(default=False)

    creation_date = models.DateTimeField(auto_now_add=True)

    def generate_token(self, length=16):
        return get_random_string(length)

    def save(self, *args, **kwargs):
        if not self.token:
            token = self.generate_token(16)
            self.token = token
        return super(PasswordResetToken, self).save(*args, **kwargs)

    @classmethod
    def create_token_for_email(cls, email):
        cls.objects.filter(email=email).update(is_expired=True)
        return cls.objects.create(email=email)

    def get_absolute_url(self):
        return f'http://{settings.SERVICE_BASE_URL}/reset-password/?e={self.email}&?t={self.token}'
