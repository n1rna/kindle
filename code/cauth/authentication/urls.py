from django.urls import path
from . import views


app_name = 'authentication'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('reset-password/', views.reset_password, name='reset_password'),
    path('verify-email/', views.verify_email, name='verify_email'),
    path('profile/', views.profile, name='profile'),
    path('user/', views.UserInfoAPIView.as_view(), name='user_info'),
]
