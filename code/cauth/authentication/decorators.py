from functools import wraps

from django.shortcuts import redirect
from django.conf import settings


def not_logged_in(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(settings.DASHBOARD_URL)
        return func(request, *args, **kwargs)
    return wrapper
