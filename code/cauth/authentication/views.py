import re

from django.shortcuts import render, redirect
from django.db.models import Q
from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth import login as django_login
from django.http import JsonResponse

from oauth2_provider.views.generic import ProtectedResourceView

from authentication.models import ExtendedUser
from authentication.decorators import not_logged_in


@not_logged_in
def login(request):
    if request.method == 'GET':
        after = request.GET.get('next', None)
        if not after:
            after = settings.LOGIN_REDIRECT_URL
        return render(request, 'login.html', {
            'next': after
        })

    if request.method == 'POST':
        username_or_email = request.POST.get('usermail', None)
        password = request.POST.get('password', None)
        after = request.POST.get('next', None)
        if not after:
            after = settings.LOGIN_REDIRECT_URL

        if not username_or_email or not password:
            return render(request, 'login.html', {
                'has_error': True,
                'error': 'Username/Email and password are both empty!'
            })

        user = ExtendedUser.objects.filter(
            Q(username=username_or_email) | Q(email=username_or_email)
        ).first()

        if not user:
            return render(request, 'login.html', {
                'has_error': True,
                'error': 'Username/Email or password is invalid.'
            })

        is_password_valid = check_password(password, user.password)
        if not is_password_valid:
            return render(request, 'login.html', {
                'has_error': True,
                'error': 'Username/Email or password is invalid.'
            })

        user.backend = 'django.contrib.auth.backends.ModelBackend'
        django_login(request, user)
        return redirect(after)


@not_logged_in
def register(request):
    if request.method == 'GET':
        return render(request, 'register.html', {})
    if request.method == 'POST':
        username = request.POST.get('username', None)
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        password2 = request.POST.get('password2', None)

        if not username or not email or not password:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'All fields are required. (Username, Email, Password)'
            })

        if password != password2:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'Passwords do not match.'
            })

        # - validate username
        username_validator = re.compile(r'^([a-zA-Z])[a-zA-Z_-]*[\w_-]*[\S]$|^([a-zA-Z])[0-9_-]*[\S]$|^[a-zA-Z]*[\S]$')
        is_username_valid = username_validator.match(username)

        if not is_username_valid:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'Username can contain alphanumeric characters, numbers and underscores(_).'
            })

        username_is_taken = ExtendedUser.objects.filter(username=username).exists()
        if username_is_taken:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'Username is taken.'
            })

        # - validate email
        email_validator = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
        is_email_valid = email_validator.match(email)
        if not is_email_valid:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'Please enter a valid email address.'
            })

        email_is_taken = ExtendedUser.objects.filter(email=email).exists()
        if email_is_taken:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'There is another account associated with this email address. Try resetting your password'
            })

        # - validate password
        password_validator = re.compile(r'^([a-zA-Z0-9@*#]{8,15})$')
        is_password_valid = password_validator.match(password)
        if not is_password_valid:
            return render(request, 'register.html', {
                'has_error': True,
                'error': 'Password should be [8-15] characters and can include [a-zA-Z0-9@*#].'
            })

        # - create user
        user = ExtendedUser.objects.create(email=email, username=username)
        user.set_password(password)
        user.save()

        return redirect('authentication:login')


def reset_password(request):
    return render(request, 'reset_password.html', {
        'has_error': True,
        'error': 'Nothing much'
    })


def verify_email(request):
    return render(request, 'verify_email.html', {})


def profile(request):
    return render(request, 'profile.html', {})


class UserInfoAPIView(ProtectedResourceView):
    def get(self, request):
        # serialize user object
        return JsonResponse({
            'first_name': request.user.first_name,
            'middle_name': request.user.middle_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
        }, status=200)
