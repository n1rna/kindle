## Running

Please add the followings to your `/etc/hosts/` file before starting the project.

```
127.0.0.1 cauth.localhost
127.0.0.1 pbackend.localhost
```

You can start the project using `docker-compose`. Please make sure you have `docker` and `docker-compose` properly installed and configured on your machine.

```
$ docker-compose up -d
```

## Startup
Start by creating a super user for `cauth` and `pbackend` services:

```
$ docker exec -ti cauth_service python manage.py createsuperuser
$ docker exec -ti pbackend_service python manage.py createsuperuser
```

Now you can create an `OAuth Application` for `pbackend`'s usage, in `cauth` service, by visiting: [http://cauth.localhost:8000/admin/oauth2_provider/application/add/](http://cauth.localhost:8000/admin/oauth2_provider/application/add/)

Make sure you set these variables right:
* Client Type: `Confidential`
* Authorization grant type: `Authorization Code`

And add the following value to `Redirect uris` field:
```
http://pbackend.localhost:8001/auth/oauth-callback/
```

Now you should enter the generated `client_id` and `client_secret` values, in `pbackend`'s [constance settings page](http://pbackend.localhost:8001/admin/constance/config/)

After creating the Application and setting `client_id` and `client_secret, you can start by visiting: [http://pbackend.localhost:8001/auth/login/](http://pbackend.localhost:8001/auth/login/)

If you need an account on our Central Authentication service, you can create one at: [http://cauth.localhost:8000/auth/register/](http://cauth.localhost:8000/auth/register/)