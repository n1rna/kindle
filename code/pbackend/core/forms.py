from django.forms import ModelForm

from .models import Author, Publisher


class AuthorForm(ModelForm):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name', 'date_of_birth', 'about']


class PublisherForm(ModelForm):
    class Meta:
        model = Publisher
        fields = ['organization_name', 'founder_name', 'founding_date', 'country']
