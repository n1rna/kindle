from rest_framework import routers

from .views import AuthorViewSet, PublisherViewSet, BookInformationViewSet

router = routers.SimpleRouter()
router.register(r'authors', AuthorViewSet, basename='author')
router.register(r'publishers', PublisherViewSet, basename='publisher')
router.register(r'books', BookInformationViewSet, basename='book')
urlpatterns = router.urls
app_name = 'core'
