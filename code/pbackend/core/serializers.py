from rest_framework import serializers

from core.models import Author, Publisher, Book


class AuthorSerializer(serializers.ModelSerializer):
    display_name = serializers.SerializerMethodField()

    def get_display_name(self, obj):
        return obj.display_name

    class Meta:
        model = Author
        exclude = ['kindle_user', 'created_by_publisher']


class PublisherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publisher
        exclude = ['kindle_user']


class BookInformationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        exclude = ['creation_date']
