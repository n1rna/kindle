from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from authentication.backends import APIAccessAuthentication

from .models import Publisher, Author, Book
from .serializers import (
    PublisherSerializer,
    AuthorSerializer,
    BookInformationSerializer
)


class PublisherViewSet(viewsets.ModelViewSet):
    serializer_class = PublisherSerializer
    authentication_classes = [APIAccessAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return Publisher.objects.filter(kindle_user=self.request.user)


class AuthorViewSet(viewsets.ModelViewSet):
    serializer_class = AuthorSerializer
    authentication_classes = [APIAccessAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return Author.objects.filter(kindle_user=self.request.user)


class BookInformationViewSet(viewsets.ModelViewSet):
    serializer_class = BookInformationSerializer
    authentication_classes = [APIAccessAuthentication, ]
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        if self.request.user.is_self_publisher:
            author = Author.objects.filter(kindle_user=self.request.user, created_by_publisher=False).first()
            return Book.objects.filter(author=author)
        publisher = Publisher.objects.filter(kindle_user=self.request.user).first()
        return Book.objects.filter(publisher=publisher)
