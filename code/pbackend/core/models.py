from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models

# Create your models here.


class Author(models.Model):
    kindle_user = models.ForeignKey('authentication.KindleUser', on_delete=models.SET_NULL, null=True)
    first_name = models.CharField(max_length=128, null=True, blank=True)
    middle_name = models.CharField(max_length=128, null=True, blank=True)
    last_name = models.CharField(max_length=128, null=True, blank=True)

    date_of_birth = models.DateField()
    avatar = models.ForeignKey('StorageImage', on_delete=models.SET_NULL, null=True)
    about = models.TextField()

    created_by_publisher = models.BooleanField(default=False)

    @property
    def display_name(self):
        return self.first_name + ' ' + self.last_name


class Publisher(models.Model):
    kindle_user = models.ForeignKey('authentication.KindleUser', on_delete=models.SET_NULL, null=True)
    organization_name = models.CharField(max_length=128, null=True, blank=True)
    founder_name = models.CharField(max_length=128, null=True, blank=True)

    country = models.CharField(max_length=64, null=True, blank=True)
    founding_date = models.DateField()


class Book(models.Model):
    primary_title = models.CharField(max_length=512, null=True, blank=True)
    secondary_title = models.CharField(max_length=512, null=True, blank=True)
    description = models.TextField()

    authors = models.ManyToManyField('Author', related_name='books')
    publisher = models.ForeignKey('Publisher', on_delete=models.SET_NULL, related_name='published_books', null=True)

    length = models.PositiveIntegerField(default=0)
    asin_code = models.CharField(max_length=128, null=True, blank=True)
    isbn_code = models.CharField(max_length=128, null=True, blank=True)

    cover_images = GenericRelation('StorageImage')

    # NOTE: we might need to limit choices
    language_code = models.CharField(max_length=10, null=True, blank=True)

    is_self_published = models.BooleanField(default=False)

    publish_date = models.DateField()
    creation_date = models.DateTimeField(auto_now_add=True)

    STATUS_CHOICES = (
        ('verified', 'verified'),
        ('unverified', 'unverified')
    )
    status = models.CharField(max_length=64, choices=STATUS_CHOICES, default='unverified')

    content_index_id = models.PositiveIntegerField(default=0)


class EditorialReviews(models.Model):
    pass


class BookAsset(models.Model):
    storage_url = models.URLField()
    title = models.CharField(max_length=128, null=True, blank=True)
    size = models.CharField(max_length=32, null=True, blank=True)
    extension = models.CharField(max_length=16, null=True, blank=True)

    creation_date = models.DateTimeField(auto_now_add=True)


class PaymentMethod(models.Model):
    title = models.CharField(max_length=128, null=True, blank=True)
    amount = models.PositiveIntegerField(default=0)

    PAYMENT_UNIT_CHOICES = (
        ('USD', 'USD'),
        ('EUR', 'EUR'),
        ('BTC', 'BTC'),
        ('LTC', 'LTC')
    )
    unit = models.CharField(max_length=16, choices=PAYMENT_UNIT_CHOICES, default='BTC')

    PAYMENT_TYPE_CHOICES = (
        ('visa', 'visa'),
        ('master', 'master'),
        ('cryptocurrency', 'cryptocurrency')
    )
    payment_type = models.CharField(max_length=64, choices=PAYMENT_TYPE_CHOICES, default='cryptocurrency')

    is_free = models.BooleanField(default=False)


class AssetPaymentAssociation(models.Model):
    book = models.ForeignKey('Book', on_delete=models.CASCADE, null=True)
    asset = models.ForeignKey('BookAsset', on_delete=models.CASCADE, null=True)
    payment = models.ForeignKey('PaymentMethod', on_delete=models.CASCADE, null=True)

    STATUS_CHOICES = (
        ('verified', 'verified'),
        ('unverified', 'unverified')
    )
    status = models.CharField(max_length=64, choices=STATUS_CHOICES, default='unverified')


class StorageImage(models.Model):
    storage_url = models.URLField()
    size = models.CharField(max_length=32, null=True, blank=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    creation_date = models.DateTimeField(auto_now_add=True)
