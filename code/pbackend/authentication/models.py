from django.db import models
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser,
    UserManager,
    PermissionsMixin
)
from django.utils.crypto import get_random_string
# Create your models here.


class KindleUser(AbstractBaseUser, PermissionsMixin):
    access_token = models.CharField(max_length=256, null=True, blank=True)
    refresh_token = models.CharField(max_length=256, null=True, blank=True)

    first_name = models.CharField(max_length=128, null=True, blank=True)
    middle_name = models.CharField(max_length=128, null=True, blank=True)
    last_name = models.CharField(max_length=128, null=True, blank=True)
    email = models.CharField(max_length=128, unique=True)
    username = models.CharField(max_length=32, unique=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = UserManager()

    associated_account_id = models.PositiveIntegerField(default=0)

    is_self_publisher = models.BooleanField(default=False)
    is_initialized = models.BooleanField(default=False)


class APIAccessToken(models.Model):
    user = models.ForeignKey('KindleUser', on_delete=models.CASCADE)
    token = models.CharField(max_length=256, unique=True)

    is_revoked = models.BooleanField(default=False)
    creation_date = models.DateTimeField(auto_now_add=True)
    revokation_date = models.DateTimeField(null=True)

    @staticmethod
    def ensure_token_for_user(user, force_new=False):
        tokens = APIAccessToken.objects.filter(user=user, is_revoked=False)
        if tokens.exists():
            if not force_new:
                return tokens.first()
            tokens.update(is_revoked=True, revokation_date=timezone.now())

        while True:
            _t = get_random_string(64)
            try:
                token = APIAccessToken.objects.create(
                    user=user,
                    token=_t
                )
                return token
            except:
                pass
