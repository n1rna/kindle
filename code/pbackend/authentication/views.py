from django.shortcuts import render, redirect
from django.conf import settings
from django.views.decorators.http import require_GET
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseBadRequest
from django.contrib.auth import login as django_login, logout as django_logout
from django.urls import reverse
from django.views import View

from .oauth_services import (
    create_authorize_url,
    exchange_grant_token,
    retrieve_user_info,
)
from .models import KindleUser, APIAccessToken
from core.forms import AuthorForm, PublisherForm
# Create your views here.


@require_GET
def login(request):
    after = request.GET.get('next', None)
    if not after:
        after = settings.SERVICE_BASE_URL

    if request.user.is_authenticated:
        return redirect(reverse('user_profile'))
    else:
        # handle redirect_url creation
        publisher_redirect_url = create_authorize_url('publisher', 'publisher')
        author_redirect_url = create_authorize_url('author', 'author')

        return render(request, 'login_with_kindle.html', {
            'publisher_redirect_url': publisher_redirect_url,
            'author_redirect_url': author_redirect_url,
        })


@login_required
def logout(request):
    # Invoke current `KindleUser`'s access/refresh token
    django_logout(request.user)
    # logout current `KindleUser`'s session
    return redirect(reverse('auth:login'))


def handle_oauth_callback(request):
    code = request.GET.get('code', None)
    state = request.GET.get('state', 'author')

    cauth_response = exchange_grant_token(code)

    if cauth_response is None or 'error' in cauth_response:
        return HttpResponseBadRequest(cauth_response.get('error', 'error'))

    access_token = cauth_response['access_token']
    user_info = retrieve_user_info(access_token)

    if not user_info or 'email' not in user_info:
        return HttpResponseBadRequest('cauth connection failed')

    user = KindleUser.objects.filter(email=user_info['email']).first()

    if user:
        APIAccessToken.ensure_token_for_user(user)
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        django_login(request, user)
        return redirect(reverse('user_profile'))

    _user_dict = {
        'access_token': cauth_response['access_token'],
        'refresh_token': cauth_response['refresh_token'],
        'first_name': user_info.get('first_name', None),
        'middle_name': user_info.get('middle_name', None),
        'last_name': user_info.get('last_name', None),
        'email': user_info.get('email', None),
        'is_self_publisher': state == 'author'
    }

    user = KindleUser.objects.create(**_user_dict)
    APIAccessToken.ensure_token_for_user(user)
    user.backend = 'django.contrib.auth.backends.ModelBackend'
    django_login(request, user)
    return redirect(reverse('user_profile'))


class ProfileView(View):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, *args, **kwargs):
        # author
        if request.user.is_self_publisher:
            if request.user.is_initialized:
                return self._normal_profile(request, user_type='author', *args, **kwargs)
            # send author initial form
            return self._initialize_form(request, user_type='author', *args, **kwargs)

        # publisher
        if request.user.is_initialized:
            return self._normal_profile(request, user_type='publisher', *args, **kwargs)
        # send publisher initial form
        return self._initialize_form(request, user_type='publisher', *args, **kwargs)

    def _normal_profile(self, request, user_type, *args, **kwargs):
        api_access = APIAccessToken.objects.filter(user=request.user, is_revoked=False).first()

        return render(request, 'profile.html', {
            'user': request.user,
            'user_type': 'author' if request.user.is_self_publisher else 'publisher',
            'api_access': api_access,
        })

    def _initialize_form(self, request, user_type, *args, **kwargs):
        form = AuthorForm

        if user_type == 'publisher':
            form = PublisherForm

        return render(request, 'profile.html', {
            'user': request.user,
            'user_type': user_type,
            'form': form
        })

    def post(self, request, *args, **kwargs):
        user = request.user
        if user.is_initialized:
            return HttpResponseBadRequest()

        user_type = 'author' if user.is_self_publisher else 'publisher',

        Form = AuthorForm
        if user_type == 'publisher':
            Form = PublisherForm

        form = Form(data=request.POST)
        if form.is_valid():
            entity = form.save()
            entity.kindle_user = user
            user.is_initialized = True
            user.save()
            entity.save()

            return render(request, 'profile.html', {'initial_success': True})

        return render(request, 'profile.html', {'form': form})
