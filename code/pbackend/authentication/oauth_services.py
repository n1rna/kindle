from urllib.parse import urlencode
import requests

from constance import config

from django.conf import settings
from django.urls import reverse


def create_authorize_url(scope, state):
    redirect_uri = settings.SERVICE_BASE_URL + reverse('auth:oauth_callback')
    qs = urlencode({
        'client_id': config.CAUTH_CLIENT_ID,
        'response_type': 'code',
        'redirect_uri': redirect_uri,
        'scope': scope,
        'state': state
    })

    return f'{settings.CAUTH_AUTHORIZE_URL}?{qs}'


def exchange_grant_token(code):
    if not code:
        return None
    try:
        redirect_uri = settings.SERVICE_BASE_URL + reverse('auth:oauth_callback')
        response = requests.post(
            settings.CAUTH_TOKEN_URL,
            data={
                'code': code,
                'grant_type': 'authorization_code',
                'redirect_uri': redirect_uri
            },
            auth=(config.CAUTH_CLIENT_ID, config.CAUTH_CLIENT_SECRET)
        )
        return response.json()
    except Exception as e:
        print(e)
        return None


def retrieve_user_info(access_token):
    try:
        headers = {'Authorization': f'Bearer {access_token}'}
        response = requests.get(
            settings.CAUTH_USER_INFO_URL,
            headers=headers
        )
        return response.json()
    except Exception as e:
        print(e)
        return None
