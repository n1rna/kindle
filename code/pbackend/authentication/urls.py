from django.urls import path

from . import views

app_name = 'authentication'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),

    path('oauth-callback/', views.handle_oauth_callback, name='oauth_callback'),
]
