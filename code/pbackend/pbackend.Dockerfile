FROM python:3
ENV PYTHONUNBUFFERED 1

RUN apt update && apt install -y netcat

WORKDIR /app
COPY requirements.txt /app/

RUN pip install -r requirements.txt

COPY ./entrypoint.sh /app/entrypoint.sh
COPY . /app

RUN chmod +x /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]