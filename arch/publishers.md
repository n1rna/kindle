# Publishers Services (Projects)

## Publishers Client(s)

> *code name: pclient*

This front-end (client) service is going to be used by all the publishers and *authors* (after MVP, we will introduce a special client for authors, which will have functionalities like Google's Docs service and cloud-based auto saving and syncing) in our system. 

### Tech Stack:

quasar vue probably - something for fast development) and after MVP, think about porting to React (TS) for more extensibility -- source code gets bigger and it is easier to apply different patterns for further maintainability of the project.

* Tests:
    * Unit tests for different components 

### MVP Considerations

This component is considered as `Critical` for the MVP release.

Features like:
* Resume Upload functionality
    * The ability to continue a stopped upload process
* Support for all doc extensions
* Auto extracting data from file (offline)

can be ignored in the MVP release.


## Publishers Backend 

> *code name: pbackend*

This backend service will be responsible for processing all requests from different clients used by *Publishers*. **Publishers Backend** receives requests and constructs a Database of all books submitted by publishers and authors. Files will be stored in a cloud object storage (S3 or S3-like solutions like minio). 

* Backup all data
* Sync with *consumers service*
* Central Authentication Integration

### Tech Stack:

This API is going to have at most 10 thousand (barely) users and probably much less in the beginning. But the reliability/stability and availablity in this service is of most concern.

Java spring boot, Python Django, ASP.NET are all good options.

* Spring-MySQL
* Django-PostgreSQL
* ASP.NET-MSSQL

Other involved components:

* Scheduled scripts for Data Syncing
* Analytics Interface (Data Interpreter and Query Generator)
    * This will include reports like "Overview" pages -- total sells, total views, etc.
* Books deduplication helper (some kind of autocomplete/suggestor while creating a new book).

* Tests:
    * Unit Tests
    * Integration tests:
        * Testing syncing scenarios with *consumers service*

### MVP Considerations

This component is considered as `Critical` for the MVP release.


## Book Parser/Loader (Extractor)

> *code name: pprocess*

We need a service for several pre-processing tasks. These tasks include:

* Checking books for copyright issues
* Extracting standard and normalized (indexable) texts from different file formats.
    * We will support different file formats, therefore we need to standardize all these texts so we would be able to do full-text searchs over those documents.
* Other tasks can be done by this service

This service acts like a pipeline that the book needs to get thorough for us to be able to consider it as an eligible book to be put on our store. It might even contain some human provisions on the content.

This service will interact with the *Publishers Backend* service's database. There are different ways for these services to interact with each other. Thes two can be kept alongside each other to be able to interact with the same database. Or we can keep them as much seperated as possible, and not relying on accessing the same database -- Using webhooks for example.

### Tech Stack

Tasks like parsing files and extracting information from them, can be very CPU consuming and IO bound. I believe the one and only `C++` is the most qualified language/tool for such tasks.

Recommended: `C++`


### MVP Considerations

This component is considered as `Critical` for the MVP release.

## Monetization

> *code name: pmoney*

There are some considerations on this part.

It is probably best to keep this part untangled to `cbackend` service. This service will act as a moderator for publishers' invoices. Publishers will have incomming invoices and will be asked for their credit card information upon registration. When they reach the point of getting paid, this service will create an invoice for the said publishers.

There are many different scenarios that might be applied to this process. Like, we might want to create invoices on specific days of month. or this can be done on a daily basis.

### Tech Stack

This services is basically a `cron` like script -- or a daemon like `while True` script. So anything will do the job.

Recommended: `Python`

* Tests:
    * We need to make sure that this script is covering our desired scenarios

### MVP Considerations

Probably not needed in the MVP

# Flows

## File upload flows

### General Overview

* Accessible through Publishers Client (Web, Desktop)
```mermaid
sequenceDiagram
    participant Publisher
    participant pclient
    participant pbackend
    Publisher->>pclient: Interact
    pclient->>+pbackend: Upload files
    pbackend->>Extractor Queue: Get queued 
    Note over pbackend,Extractor Queue: to be processed by <br> `Parser` component
    pbackend-->>-Publisher: Response
```

### Bulk Upload

* For this feature, we should have some APIs ready to receive bulk requests and ready explained documentations for publishers to be able to integrate their current databases to our platform.
* Some people might need support for this feature and maybe we need to have a support team for those customers.


```mermaid
sequenceDiagram
    participant Customer
    participant Publishers API
    participant Extractor Queue
    Customer->>+Publishers API: Create Book Entity <br> and upload file
    Publishers API->>Extractor Queue: Get queued 
    Note over Publishers API,Extractor Queue: to be processed by <br> `Parser` component
    Publishers API-->>-Customer: Response

```

### Book Creation (2-Step)

* Create Book object -- fill information about book and author
* Edit Book object information
* Upload files for book
    * choose payment method for each file
    * files get stored in the object storage (minio)


```mermaid
sequenceDiagram
    participant Customer
    participant Publishers API

    Customer->>+Publishers API: Create Book Entity
    Publishers API-->>-Customer: Response
```

```mermaid
sequenceDiagram
    participant Customer
    participant Publishers API
    participant Object Storage
    participant RabbitMQ

    Customer->>Publishers API: Upload File
    Note over Customer,Publishers API: Including payment method info
    Publishers API->>+Object Storage: Upload File
    Object Storage-->>-Publishers API: Response
    Publishers API-->>RabbitMQ: Enqueue asset for processing
    Publishers API->>Customer: Response
```

```mermaid
sequenceDiagram
    participant pprocessor
    participant RabbitMQ
    participant Index Engine
    participant pbackend

    pprocessor-->>RabbitMQ: Consume
    RabbitMQ->>+pprocessor: Unprocessed assets
    pprocessor-->>Index Engine: Index content here
    pprocessor->>-pbackend: Extract info and ack pbackend
    Note over pprocessor,pbackend: Extract asset content and index them to index engine
```

## Analytics Reports

* Overview of all statistics
* Reports about different KPIs and metrics

```mermaid
sequenceDiagram
    participant Publisher
    participant Management Panel
    participant Analytics Interface
    participant Analytics Service
    Publisher->>Management Panel: Upload files
    Management Panel->>Analytics Interface: get information
    Analytics Interface->>+Analytics Service: sync information
    Analytics Service-->>-Analytics Interface: 
    Analytics Interface-->>Publisher: Response (reports)
```


## Data Integration (With Consumers Backend)

After the file goes thorugh the `pprocess` pipeline, 3 different palces get updated.
1. Record in `pbackend` database.
2. Raw file uplaoded to `cdn` and linked to the previous record.
3. Extracted information stored somewhere for further usage. (elastic)


```mermaid
sequenceDiagram
    participant cbackend
    participant pbackend
    participant cbackend database

    cbackend->>+pbackend: Retrieve news
    pbackend-->>-cbackend: Get New Information
    cbackend->>cbackend database: Store Information
    Note over cbackend,cbackend database: Serialize information properly for consumers' usage.
```
