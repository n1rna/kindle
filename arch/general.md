# Kindle

The Amazon Kindle is a series of e-readers designed and marketed by Amazon.

This platform is a two-sided product, therefore without each of these parties, our platform will not work and will have no insentives for neither of parties.

* `Publishers` and `Authors`
* `Consumers` (Readers)

Considering the above fact, even in our MVP stage, we should be able to provide functionalities for both parties. Almost all of the services mentioned thorough this document, are required to satisfy all the functionalities needed in the MVP stage. But for the sake of less development cost and implementation time while working on the MVP, we have pointed out all the considerations and less important features in each service's descriptions.

*IMPORTANT NOTICE*

Some of the mentioned services and products, might get mixed with each other for faster development and less engineering complexity.

## List of all services

* Publishers
    * pclient
        * pwriter
    * pbackend
    * pprocess
    * pmoney
* Consumers
    * cclient
    * cbackend
    * creader

## Priorities

As we have mentioned before, we can not prioritize any of the two sides over the other one. The below prioritization has been done based on engineering limitations and decisions might change due to variables like "How big is the development team?", etc.

> We have assigned an value as importance factor, to each part. Below is the notation helper:
> ```
> Critical - c
> High - h
> Normal - n
> Optional - o
> ```

1. (`h`) Designing Client Interfaces
    * Publishers dashboard
    * Store front
    * Consumers dashboard
2. Backend Development
    * (`c`) Central Authentication - `cauth`
    * (`c`) Publishers API - `pbackend`
        * (`c`) Used by clients
        * (`n`) Public APIs
    * (`n`) Monetization
    * (`c`) Consumers - `cbackend`
        * (`c`) E-Commerce backend
        * (`h`) Tracking
3. Clients Development
    * (`h`) Publishers - `pclient`
    * (`h`) Store front - `cclient`
    * (`h`) Consumers dashboard - `cclient`
4. (`h`) Infrastructure Implementation

## MVP

As our Minimum Viable Product, we should be able to provide both parties in our platform, with the minimum functionality flows.

For our `Publishers` we should have the following bare minimums:

* Add new authors
* Add books using the `Publishers dashboard`
* Bulk publish books (using APIs)
* Track sells, views (+ invoices)
* Select and customize payment plans for each book (or group of books)

For our `Consumers` we should have the following bare minimums:

* Searching titles, publishers, authors, genres, etc in the store front
* Buying books
    * Shopping Cart
    * Payment
    * Rental -- Optional
    * Gift
* Recommendations
    * Based on buys
    * Based on browsing history
    * Genre Trends
* Managing bought books (Library)
    * Wishlist -- Optional
* Tracking readings

In the MVP stage, we will treat `Authors` very similar to `Publishers`. Until we can get our `Writer` client ready for authors, we will just treat them like *Independent* publishers who are publishing their own work and can keep track of sells, views, etc.
