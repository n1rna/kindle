# Authentication Services

## Central Kindle Auth

> *code name: cauth*

For the sake of simplicity in other services and applications that we are going to have in our product ecosystem, we will design a central authentication service, which will then provide our other services with OAuth functionalities.

### Applications

For now, we will have just `two` applications in our ecosystem.

* `pbackend`
* `cbackend`

Users of each of these applications, will be redirected to a central auth service to get authenticated.

### Morale

Using this pattern, we can both keep our different types of users (`authors`, `publishers`, `consumers`) separated and not have tangled auth logics, and in case any of our users wants to have a mixed interaction with our platform (an `auhtor` who is a `consumer` too), can use same credentials for both of his/her accounts.


# Flows

## OAuth2

* Resource Providers:
    * `pbackend`
    * `cbackend`

```mermaid
sequenceDiagram
    participant User
    participant Resource Provider
    participant Auth Provider

    User->>Resource Provider: Request Authentication
    Resource Provider-->>User: Redirect Url
    User->>Auth Provider: Get Redirected
    Auth Provider-->>Resource Provider: Grant Access
    Resource Provider->>+Auth Provider: Issue Access Token <br> and authenticate user
    Auth Provider-->>-Resource Provider: 
    Resource Provider-->>User: Redirect to client with <br> logged in session
```

