# Consumers Services (Projects)

Most of our users will be using this side of kindle platform. Unlike our `Publishers` side, this side will have many users just surfing the products without even buying or using any of them.

* High amount of search requests
    * logging
    * session-based recommendation

Although most of the times that users are using our clients, they will be offline and very few online interactions will exist between clients and our backend, we will have many online users on our platform at each moment.

## Store Front

> *code name: cclient*

* Shop
    * All functionalities for a ecommerce website
* User Dashboard
    * Managing bought books
    * Managing logged in devices
    * Reading tracking (keeping track of every book started)
    * Applying for different programs
        * Kindle Unlimited
* i18n

### Tech Stack:

Honestly, I think choosing the front-end stack relies very much on the development team and their skills.

I have mostly worked with `ReactJS` and am familiar with it's internals. I have also used `vue` and `angular`, but not for a production grade product. The biggest differences are in the frameworks' implementation and not in the usage.

**Server Side Rendering** is very important for this client. Because the content should be indexable by search engines and this should be a priority in mind. `React` uses `next` as an addon to handle SSR and in `vue` we have `nuxt`. Both `next` and `nuxt` have their built-in customized `express` server, and both let the user choose any other backend (nodejs backends like `koa`, `micro`, etc) for handling SSR requests.

Another point that I think should be considered in choosing one of these front-end frameworks over the other ones, is **Maintainablity**. I believe when you work with `ReactJS` you have all the power to do things your way, which can be confusing some times. But when the project gets bigger and many people should work on it, the extensibility that `ReactJS` gives us, can save many lives! I have worked with `vue` far less than `ReactJS`, but I think the structural limitations that `vue` applies, for it to be able to interpret different syntaxes and such, will tie our hands in the future. So my final suggestion would be `ReactJS` over other front-end frameworks.


* Tests:

### MVP Considerations:

This component is considered as `Critical` for the MVP release.


## Reader Front

> *code name: creader*

This client will be installed on all kindle devices. This is like an application in which you can access all your books bought on `cclient` and read them with a very good experience.

This book reader will enable you to keep track of all your readings, will give you an experience much like reading a physical book (paginating, etc)

Other functionalities like "Buying books" and "Browsing kindle store" can get embedded inside this client too. *But*, we can just redirect users to `cclient` for buying books and going back to `creader` after buying their book.

### Tech Stack:
* Tests:

### MVP Considerations:

We are not going to release this product any time soon. This product's use cases and audience should be investigated and researched more before even choosing technology stack or starting the development.

## Store Backend

> *code name: cbackend*

* Search
* Recommendation
* Buying books
* Authorization for bought books and handling different subscribed programs
* Shopping Cart
* Wishlist
* Book reading tracking (logging)
* Reading Analytics

### Tech Stack:

This backend is going to handle many, many requests for different tasks. I believe this whole service that we are talking about right now, is going to be seperated to smaller micro-services in the near future. Components like `Search`, `Recommendation` can become big components.

Choosing a language/framework for such components, can either be very complicated or very simple. Theoretically, any language/framework can be a suitable choice and can be coded/tuned/deployed in a way that can handle all the requests. But on the other hand, development team's resources (skills) and preferences are the most important parameters.

I would suggest Java/Spring for this project. Deployed properly in a distributed environment, we can scale as much as needed.

* Tests:

### MVP Considerations:

This component is considered as `Critical` for the MVP release.
